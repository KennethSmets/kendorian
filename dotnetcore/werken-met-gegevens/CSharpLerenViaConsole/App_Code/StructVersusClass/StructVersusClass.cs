﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaConsole.App_Code.StructVersusClass
{
    class StructVersusClass
    {
        static void Main(string[] args)
        {
            Auto auto;

            auto = new Auto("Blauw");
            Console.WriteLine(auto.Beschrijf());

            auto = new Auto("Rood");
            Console.WriteLine(auto.Beschrijf());

            Console.ReadKey();
        }

        struct Auto
        {
            private string kleur;

            public Auto(string kleur)
            {
                this.kleur = kleur;
            }

            public string Beschrijf()
            {
                return "Deze auto is " + Kleur;
            }

            public string Kleur
            {
                get { return kleur; }
                set { kleur = value; }
            }
        }


    }
}
