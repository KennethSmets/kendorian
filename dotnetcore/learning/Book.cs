﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCore.Learning
{
    class Book
    {

        private static string title;
        public static string Title
        {

            get
            {
                return title;
            }
            set
            {
                title = value;
            }

        }

        private static string year;
        public static string Year
        {

            get
            {
                return year;
            }
            set
            {
                year = value;
            }

        }

        private static string city;
        public static string City
        {

            get
            {
                return city;
            }
            set
            {
                city = value;
            }

        }

        private static string publisher;
        public static string Publisher
        {

            get
            {
                return publisher;
            }
            set
            {
                publisher = value;
            }

        }

        private static string author;
        public static string Author
        {

            get
            {
                return author;
            }
            set
            {
                author = value;
            }

        }

        private static string edition;
        public static string Edition
        {

            get
            {
                return edition;
            }
            set
            {
                edition = value;
            }

        }

        private static string translator;
        public static string Translator
        {

            get
            {
                return translator;
            }
            set
            {
                translator = value;
            }

        }

        private static string comment;
        public static string Comment
        {

            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }

        }

    }
}
