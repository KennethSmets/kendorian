﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Data;

namespace DotNetCore.Learning
{
    class BookSerializing
    {

        //[XmlRoot("E:\\gitroot\\programmeren3\\dotnetcore\\learning\\Data\\Book.xml")]

        public class BookList
        {
            public BookList() { Items = new List<Book>(); }
            [XmlElement("Book")]
            public List<Book> Items { get; set; }
        }

        public class Book
        {
            [XmlElement("Title")]
            public Int32 Title { get; set; }

            [XmlElement("Year")]
            public String Year { get; set; }

            [XmlElement("City")]
            public String City { get; set; }

            [XmlElement("Publisher")]
            public String Publisher { get; set; }

            [XmlElement("Author")]
            public String Author { get; set; }

            [XmlElement("Edition")]
            public String Edition { get; set; }

            [XmlElement("Translator")]
            public String Translator { get; set; }

            [XmlElement("Comment")]
            public String Comment { get; set; }
        }

        public static void DeserializeFromXML()
        {
            var myDeserializer = new XmlSerializer(typeof(List<Book>));
            List<Book> ToDoList;
            using (var myFileStream = new FileStream("E:\\gitroot\\programmeren3\\dotnetcore\\learning\\Data\\Book.xml", FileMode.Open))
            {
                ToDoList = (List<Book>)myDeserializer.Deserialize(myFileStream);
            }
        }



    }

    
}
