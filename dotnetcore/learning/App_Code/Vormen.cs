﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    public class Vormen
    {        
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        private Char karakter;
        public Char Karakter
        {

            get { return karakter; }
            set
            {
                karakter = value;
            }
        }


        //----------------LIJN----------------

        public static string Lijn(int lengte)
        {
            Vormen lijn = new Vormen();
            lijn.karakter = '*';

            Console.ForegroundColor = ConsoleColor.White;
            return new string(lijn.karakter,lengte);
        }

        public static string Lijn(int lengte, char karakter)
        {
            Vormen lijn = new Vormen();
            lijn.karakter = karakter;

            Console.ForegroundColor = ConsoleColor.White;
            return new string(lijn.karakter, lengte);
        }

        public static string Lijn(int lengte, ConsoleColor kleur)
        {
            Vormen lijn = new Vormen();
            lijn.karakter = '*';

            Console.ForegroundColor = kleur;
            return new string(lijn.karakter, lengte);
        }

        public static string Lijn(int lengte, char karakter, ConsoleColor kleur)
        {
            Vormen lijn = new Vormen();
            lijn.karakter = karakter;

            Console.ForegroundColor = kleur;
            return new string(lijn.karakter, lengte);
        }


        //-----------------------DRIEHOEK----------------------------

        public static void Driehoek(int lengtebasis)
        {
            Vormen driehoek = new Vormen();
            driehoek.karakter = '*';
            Console.ForegroundColor = ConsoleColor.White;

            int i, j;
            for (i = 1; i <= lengtebasis; i++)
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(driehoek.karakter);
                }
                Console.WriteLine("");
            }

        }

        public static void Driehoek(int lengtebasis, char karakter)
        {
            Vormen driehoek = new Vormen();
            driehoek.karakter = karakter;
            Console.ForegroundColor = ConsoleColor.White;

            int i, j;
            for (i = 1; i <= lengtebasis; i++)
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(driehoek.karakter);
                }
                Console.WriteLine("");
            }

        }

        public static void Driehoek(int lengtebasis, ConsoleColor kleur)
        {
            Vormen driehoek = new Vormen();
            driehoek.karakter = '*';
            Console.ForegroundColor = kleur;

            int i, j;
            for (i = 1; i <= lengtebasis; i++)
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(driehoek.karakter);
                }
                Console.WriteLine("");
            }

        }

        public static void Driehoek(int lengtebasis, char karakter, ConsoleColor kleur)
        {
            Vormen driehoek = new Vormen();
            driehoek.karakter = karakter;
            Console.ForegroundColor = kleur;

            int i, j;
            for (i = 1; i <= lengtebasis; i++)
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(driehoek.karakter);
                }
                Console.WriteLine("");
            }

        }


        //----------------------RECHTHOEK---------------------
        
        static void RechthoekLijn(int w, char einde, char mid)
        {
            Console.Write(einde);
            for (int i = 1; i < w - 1; ++i)
                Console.Write(mid);
            Console.WriteLine(einde);
        }

        public static void Rechthoek(int breedte, int hoogte)
        {
            Vormen rechthoek = new Vormen();
            rechthoek.karakter = '*';
            Console.ForegroundColor = ConsoleColor.White;

            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);
            for (int i = 1; i < hoogte - 1; ++i)
            {
                RechthoekLijn(breedte, rechthoek.karakter, ' ');
            }
            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);
            

        }

        public static void Rechthoek(int breedte, int hoogte, char karakter)
        {
            Vormen rechthoek = new Vormen();
            rechthoek.karakter = karakter;
            Console.ForegroundColor = ConsoleColor.White;

            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);
            for (int i = 1; i < hoogte - 1; ++i)
            {
                RechthoekLijn(breedte, rechthoek.karakter, ' ');
            }
            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);


        }

        public static void Rechthoek(int breedte, int hoogte, ConsoleColor kleur)
        {
            Vormen rechthoek = new Vormen();
            rechthoek.karakter = '*';
            Console.ForegroundColor = kleur;

            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);
            for (int i = 1; i < hoogte - 1; ++i)
            {
                RechthoekLijn(breedte, rechthoek.karakter, ' ');
            }
            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);


        }

        public static void Rechthoek(int breedte, int hoogte, char karakter, ConsoleColor kleur)
        {
            Vormen rechthoek = new Vormen();
            rechthoek.karakter = karakter;
            Console.ForegroundColor = kleur;

            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);
            for (int i = 1; i < hoogte - 1; ++i)
            {
                RechthoekLijn(breedte, rechthoek.karakter, ' ');
            }
            RechthoekLijn(breedte, rechthoek.karakter, rechthoek.karakter);


        }

    }
}
